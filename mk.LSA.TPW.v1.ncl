load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/time_axis_labels.ncl"
load "$NCARG_ROOT/custom_functions.ncl"
begin
	
	sa = "lsan"
	
	ver = "2b"
;====================================================================================================
; Load Sounding Array Data
;====================================================================================================
	;idir = "~/Research/DYNAMO/LSA/data/"
	idir = "/data2/whannah/DYNAMO/LSA/data/"
	if ver.eq."2a" then idir = idir+"v2a_obsonly/" end if
	if ver.eq."2b" then idir = idir+"v2b_withECOA/" end if

	infile = addfile(idir+"dynamo_basic_v"+ver+"_2011all.nc" ,"r")
	ps  = infile->ps;(:,{lat1:lat2},{lon1:lon2})
	p   = infile->level
	p!0 = "lev"
	p&lev = p
	dp  = calc_dP(p,ps) *100.
	;u   = infile->u;(:,:,{lat1:lat2},{lon1:lon2})
	;v   = infile->v;(:,:,{lat1:lat2},{lon1:lon2})
	wmr = infile->q;(:,:,{lat1:lat2},{lon1:lon2})
	wmr = (/wmr/1000./)
	q 	= wmr/(1.+wmr)
	copy_VarCoords(wmr,q)
	dt 	= 3*3600.
	
	;adv = calc_adv(q,u,v) *-1.
	;ddt = calc_ddt(q,dt)
	;cpr = (ddt-adv)

	SATPW = dim_sum_n(q*dp/g,1)

	copy_VarCoords(q(:,0,:,:),SATPW)
	printVarSummary(SATPW)
	;exit

	;SADDT = block_avg( dim_avg_n( ddt ,(/2,3/)) ,8)
	;SAADV = block_avg( dim_avg_n( adv ,(/2,3/)) ,8)
	;SACPR = block_avg( dim_avg_n( cpr ,(/2,3/)) ,8)
	
	;SAADV = (/SAADV *86400./)
	;SADDT = (/SADDT *86400./)
	;SACPR = (/SACPR *86400./)
	
	;do n = 0,nsmooth-1 SACPR = wgt_runave_n_Wrap(SACPR, (/0.25,0.5,0.25/), 0,0) end do
	;do n = 0,nsmooth-1 SADDT = wgt_runave_n_Wrap(SADDT, (/0.25,0.5,0.25/), 0,0) end do
	;do n = 0,nsmooth-1 SAADV = wgt_runave_n_Wrap(SAADV, (/0.25,0.5,0.25/), 0,0) end do
	
	;SATPW!0 = "time"
	;SATPW!1 = "lat"
	
	;delete([/u,v,wmr,q,dt,adv,ddt,cpr/])
;====================================================================================================
;====================================================================================================
	ofile = idir+"dynamo_TPW_v"+ver+"_2011all.nc"
	if isfilepresent(ofile) then system("rm "+ofile) end if
	outfile = addfile(ofile,"c")
	outfile->TPW = SATPW
	
	print("	"+ofile)
;====================================================================================================
;====================================================================================================
end
